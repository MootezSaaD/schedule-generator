# Schedule Generator

This is part of the CS321 course at SMU-MedTech.
It is an automatic schedule generator using Genetic Algorithms. It is inspired from Genetic Algorithms in Java Basics by Lee Jacobson and Burak Kanber.

# Version 0.1
<p>Algorithm wise, the software yields schedules that do not violate the constraints given by the user.<br>
However, the data (classrooms, professors and courses) at this stage is hardcoded, in further releases the software would allow users to
upload CSV files.</p>

# Contributors
<ul>
<li>Mootez Saad</li>
<li>Mahdi Turki</li>
<li>Marwen Dallel</li>
<li>Zeineb Moalla</li>
</ul>

