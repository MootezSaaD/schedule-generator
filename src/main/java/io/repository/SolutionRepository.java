package io.repository;

import io.algorithm.Schedule;

public class SolutionRepository {
  private Schedule setOfSolutions;

  private static SolutionRepository repository = null;

  public static SolutionRepository getInstance() {
    if (repository == null) {
      repository = new SolutionRepository();

    }
    return repository;
  }

  private SolutionRepository() {
  }

  public void storeSolution(Schedule sols) {
    this.setSetOfSolutions(sols);
  }

  public Schedule getSetOfSolutions() {
    return setOfSolutions;
  }

  public void setSetOfSolutions(Schedule setOfSolutions) {
    this.setOfSolutions = setOfSolutions;
  }
  
}
