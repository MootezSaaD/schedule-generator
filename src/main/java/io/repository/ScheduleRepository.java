package io.repository;

import java.awt.FileDialog;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import io.algorithm.Schedule;
import io.entities.Instructor;
import io.entities.Classroom;
import io.entities.Course;

public class ScheduleRepository extends Schedule {
	private Schedule schedule;
	private static ScheduleRepository scheduleRepository = null;

	public static ScheduleRepository getInstance() {
		if (scheduleRepository == null) {
			scheduleRepository = new ScheduleRepository();
		}
		return scheduleRepository;
	}
	
	private ScheduleRepository() {
		super();
		this.schedule = new Schedule();
	};
	
	public Schedule getTimetable() {
		return schedule;
	}

	public void setTimetable(Schedule timetable) {
		this.schedule = timetable;
	}

	public void readFromCSVProf() {
		FileDialog fd = new FileDialog(new JFrame());
		fd.setVisible(true);
		try (Reader reader = Files.newBufferedReader(Paths.get(fd.getFiles()[0].getAbsolutePath()));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);) {

			for(CSVRecord record: csvParser) {
				String firstName = record.get(0);
				String lastName = record.get(1);
				schedule.addInstructor(firstName + " " + lastName);
			}
			
			schedule.getInstructors().keySet().forEach(new Consumer<Integer>() {
				@Override
				public void accept(Integer professorID) {
					System.out.println(schedule.getInstructor(professorID).getInstructorName());
				}
			});
			
		} catch (IOException err) {
			JOptionPane.showMessageDialog(null, "File Not Found", "Error", 1, null);
		} catch(ArrayIndexOutOfBoundsException e) {
		}
	}

	public void readFromCSVModule() {
		FileDialog fd = new FileDialog(new JFrame());
		fd.setVisible(true);
		try (Reader reader = Files.newBufferedReader(Paths.get(fd.getFiles()[0].getAbsolutePath()));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);) {

			for(CSVRecord record: csvParser) {
				String courseCode = record.get(0);
				String courseName = record.get(1);

				String professorIDsStr = record.get(2);
				//System.out.println(professorIDsStr);
				Pattern pattern = Pattern.compile("(\\d+)");
				Matcher matcher = pattern.matcher(professorIDsStr);
				ArrayList<Integer> professorIDs = new ArrayList<Integer>();

				// Find all matches
				while (matcher.find()) {
					professorIDs.add(Integer.parseInt(matcher.group()));
				}

				schedule.addCourse(courseCode, courseName, professorIDs.stream().mapToInt(i -> i).toArray());
			}

		} catch (IOException err) {
			JOptionPane.showMessageDialog(null, "File Not Found", "Error", 1, null);
		} catch (ArrayIndexOutOfBoundsException err) {

		}
	}

	public void readFromCSVRoom() {
		FileDialog fd = new FileDialog(new JFrame());
		fd.setVisible(true);
		try (Reader reader = Files.newBufferedReader(Paths.get(fd.getFiles()[0].getAbsolutePath()));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);) {

			for(CSVRecord record: csvParser) {
				String roomName = record.get(0);
				int capacity = Integer.parseInt(record.get(1));
				schedule.addClassroom(roomName, capacity);
			}

		} catch (IOException err) {
			JOptionPane.showMessageDialog(null, "File Not Found", "Error", 1, null);
		} catch (ArrayIndexOutOfBoundsException err) {

		}
	}

	public Object[][] prof2Table() {
		HashMap<Integer, Instructor> professors = schedule.getInstructors();
		Object[][] data = new Object[schedule.getInstructors().size()][1];
		for (int i = 0; i < schedule.getInstructors().size(); i++) {
			data[i][0] = professors.get(i+1).getInstructorName();
		}
		return data;
	}

	public Object[][] module2Table() {
		HashMap<Integer, Course> courses = schedule.getCourses();
		Object[][] data = new Object[schedule.getCourses().size()][3];
		for (int i = 0; i < schedule.getCourses().size(); i++) {
			data[i][0] = courses.get(i+1).getCourseCode();
			data[i][1] = courses.get(i+1).getCourseName();
			// THIS IS NOT SERIOUS
			data[i][2] = courses.get(i+1).fetchRandomInstructorId();
		}
		return data;
	}

	public Object[][] room2Table() {
		HashMap<Integer, Classroom> classrooms = schedule.getClassrooms();
		Object[][] data = new Object[schedule.getClassrooms().size()][2];
		for (int i = 0; i < schedule.getClassrooms().size(); i++) {
			data[i][0] = classrooms.get(i+1).getClassroomName();
			data[i][1] = classrooms.get(i+1).getClassroomCapacity();
		}
		return data;
	}
}