package io.algorithm;
import io.entities.Cohort;
import io.entities.Course;

public class Individual {
	
	private double currentFitness = -1;
	private int[] chromosomeArr;
	
	public Individual(Schedule schedule) {
		int classesCount = schedule.getNumberOfClasses();
		int idxChromosome = 0;
		int lenChromosome = classesCount * 3;
		int chromosome[] = new int[lenChromosome];
		for (Cohort group : schedule.getCohortArr()) {
			for (int courseId : group.getCourseIds()) {
				int scheduleId = schedule.getRandomScheduleslot().getScheduleslotId();
				chromosome[idxChromosome] = scheduleId;
				idxChromosome++;

				int classroomId = schedule.pickRandomClassroom().getClassroomId();
				chromosome[idxChromosome] = classroomId;
				idxChromosome++;

				Course course = schedule.getCourse(courseId);
				chromosome[idxChromosome] = course.fetchRandomInstructorId();
				idxChromosome++;
			}
		}
		this.chromosomeArr = chromosome;
	}

	public Individual(int lenChromosome) {
		int[] indv;
		indv = new int[lenChromosome];
		
		for (int idxGene = 0; idxGene < lenChromosome; idxGene++) {
			indv[idxGene] = idxGene;
		}
		
		this.chromosomeArr = indv;
	}
    
	
	public Individual(int[] arrChrom) {
		this.chromosomeArr = arrChrom;
	}

	
	public int[] getChromosome() {
		return this.chromosomeArr;
	}

	
	public int getLenChromosome() {
		return this.chromosomeArr.length;
	}

	
	public void setGene(int idx, int gene) {
		this.chromosomeArr[idx] = gene;
	}

	
	public int getGene(int idx) {
		return this.chromosomeArr[idx];
	}

	
	public void setFitness(double fitness) {
		this.currentFitness = fitness;
	}

	
	public double getFitness() {
		return this.currentFitness;
	}
	
	public String toString() {
		String str = "";
		for (int idxGene = 0; idxGene < this.chromosomeArr.length; idxGene++) {
			str += this.chromosomeArr[idxGene] + ",";
		}
		return str;
	}

	
	public boolean hasGene(int gene) {
		for (int i = 0; i < this.chromosomeArr.length; i++) {
			if (this.chromosomeArr[i] == gene) {
				return true;
			}
		}
		return false;
	}


	
}
