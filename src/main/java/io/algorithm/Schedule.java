package io.algorithm;


import java.util.HashMap;

import io.entities.Class;
import io.entities.Cohort;
import io.entities.Course;
import io.entities.Instructor;
import io.entities.Classroom;
import io.entities.Scheduleslot;


public class Schedule {
	private final HashMap<Integer, Classroom> classrooms;
	private final HashMap<Integer, Instructor> instructors;
	private final HashMap<Integer, Scheduleslot> scheduleslots;
	private final HashMap<Integer, Course> courses;
	private final HashMap<Integer, Cohort> cohorts;
	
	private Class classes[];
	private int classesCount = 0;

	public Schedule() {
		this.classrooms = new HashMap<Integer, Classroom>();
		this.instructors = new HashMap<Integer, Instructor>();
		this.courses = new HashMap<Integer, Course>();
		this.cohorts = new HashMap<Integer, Cohort>();
		this.scheduleslots = new HashMap<Integer, Scheduleslot>();
	}

	
	public Schedule(Schedule schedule) {
		this.classrooms = schedule.getClassrooms();
		this.instructors = schedule.getInstructors();
		this.courses = schedule.getCourses();
		this.cohorts = schedule.getCohorts();
		this.scheduleslots = schedule.getScheduleslots();
	}

	public HashMap<Integer, Cohort> getCohorts() {
		return this.cohorts;
	}

	public HashMap<Integer, Scheduleslot> getScheduleslots() {
		return this.scheduleslots;
	}

	public HashMap<Integer, Course> getCourses() {
		return this.courses;
	}

	public HashMap<Integer, Instructor> getInstructors() {
		return this.instructors;
	}

	
	public void addClassroom(String name, int maxSize) {
		Classroom classrooms = new Classroom(name, maxSize);
		this.classrooms.put(classrooms.getClassroomId(), classrooms);
	}

	
	public void addInstructor(String name) {
		Instructor instructor = new Instructor(name);
		this.instructors.put(instructor.getInstructorId(), instructor);
	}

	
	public void addCourse(String courseCode, String courseName, int instructorIds[]) {
		Course course = new Course(courseCode, courseName, instructorIds);
		this.courses.put(course.getCourseId(), course);
	}

	
	public void addCohort(int cohortSize, int courseIds[]) {
		Cohort cohort = new Cohort(cohortSize, courseIds);
		this.cohorts.put(cohort.getCohortId(), cohort);
		this.classesCount = 0;
	}

	
	public void addScheduleslot(String scheduleslotStr) {
		Scheduleslot scheduleslot = new Scheduleslot(scheduleslotStr);
		this.scheduleslots.put(scheduleslot.getScheduleslotId(), scheduleslot);
	}

	
	public void generateClasses(Individual idv) {
		int chrPos = 0;
		int idxClass = 0;
		
		Class newClasses[] = new Class[this.getNumberOfClasses()];
		int chr[] = idv.getChromosome();

		for (Cohort cohort : this.getCohortArr()) {
			int courseIds[] = cohort.getCourseIds();
			for (int courseId : courseIds) {
				newClasses[idxClass] = new Class(idxClass, cohort.getCohortId(), courseId);
				
				newClasses[idxClass].addScheduleslot(chr[chrPos]);
				chrPos++;
				newClasses[idxClass].setClassroomId(chr[chrPos]);
				chrPos++;
				newClasses[idxClass].addInstructor(chr[chrPos]);
				chrPos++;

				idxClass++;
			}
		}
		this.classes = newClasses;
	}

	
	public Classroom getClassroom(int classroomId) {
		if (!this.classrooms.containsKey(classroomId)) {
			System.out.println("Classrooms has no key " + classroomId);
		}
		return (Classroom) this.classrooms.get(classroomId);
	}

	public HashMap<Integer, Classroom> getClassrooms() {
		return this.classrooms;
	}

	
	public Classroom pickRandomClassroom() {
		Object[] classroomsArr = this.classrooms.values().toArray();
		Classroom classroom = (Classroom) classroomsArr[(int) (classroomsArr.length * Math.random())];
		return classroom;
	}

	public Instructor getInstructor(int instructorId) {
		return (Instructor) this.instructors.get(instructorId);
	}

	
	public Course getCourse(int courseId) {
		return (Course) this.courses.get(courseId);
	}

	
	public int[] getCohortCourses(int cohortId) {
		Cohort cohort = (Cohort) this.cohorts.get(cohortId);
		return cohort.getCourseIds();
	}
	
	public Cohort getCohort(int cohortId) {
		return (Cohort) this.cohorts.get(cohortId);
	}

	public Cohort[] getCohortArr() {
		return (Cohort[]) this.cohorts.values().toArray(new Cohort[this.cohorts.size()]);
	}

	public Scheduleslot getScheduleslot(int scheduleslotId) {
		return (Scheduleslot) this.scheduleslots.get(scheduleslotId);
	}
	
	public Scheduleslot getRandomScheduleslot() {
		Object[] scheduleslotArr = this.scheduleslots.values().toArray();
		Scheduleslot scheduleslot = (Scheduleslot) scheduleslotArr[(int) (scheduleslotArr.length * Math.random())];
		return scheduleslot;
	}
	
	public Class[] getClasses() {
		return this.classes;
	}
	
	public int getNumberOfClasses() {
		if (this.classesCount > 0) {
			return this.classesCount;
		}

		int classesCount = 0;
		Cohort cohorts[] = (Cohort[]) this.cohorts.values().toArray(new Cohort[this.cohorts.size()]);
		for (Cohort cohort : cohorts) {
			classesCount += cohort.getCourseIds().length;
		}
		this.classesCount = classesCount;

		return this.classesCount;
	}

	
	public int findConflicts() {
		int conflicts = 0;

		for (Class classX : this.classes) {
			int classroomCap = this.getClassroom(classX.getClassroomId()).getClassroomCapacity();
			int sizeCohort = this.getCohort(classX.getCohortId()).getCohortSize();
			
			if (classroomCap < sizeCohort) {
				conflicts++;
			}
	
			for (Class classY : this.classes) {
				if (classX.getClassroomId() == classY.getClassroomId() && classX.getScheduleslotId() == classY.getScheduleslotId()
						&& classX.getClassId() != classY.getClassId()) {
					conflicts++;
					break;
				}
			}

			for (Class classY : this.classes) {
				if (classX.getInstructorId() == classY.getInstructorId() && classX.getScheduleslotId() == classY.getScheduleslotId()
						&& classX.getClassId() != classY.getClassId()) {
					conflicts++;
					break;
				}
			}
		}

		return conflicts;
	}
}