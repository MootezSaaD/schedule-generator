package io.algorithm;
import io.entities.Class;
import io.repository.SolutionRepository;


public class RunConsole {
    public static SolutionRepository solutions;
    public static  Schedule schedule;
    public static void main() {
    	solutions = SolutionRepository.getInstance();
    	schedule = createSchedule();
        GA algorithm = new GA(250, 0.02, 0.9, 2, 5); 
        Population pop = algorithm.firstPopulation(schedule);
        algorithm.assessPop(pop, schedule); 
        int gen = 1;
 
        while (algorithm.hasReachedMaxGen(gen, 1000) == false
            && algorithm.hasReachedMaxFitness(pop) == false) {  
            System.out.println("Gen #" + gen + " Highest fitness: " + pop.getMostFit(0).getFitness()); 
            pop = algorithm.crossover(pop);
            pop = algorithm.mutatePop(pop, schedule); 
            algorithm.assessPop(pop, schedule);
            gen++;
        }

        schedule.generateClasses(pop.getMostFit(0));
        System.out.println();
        System.out.println("Solution found in " + gen + " generations");
        System.out.println("Final solution fitness: " + pop.getMostFit(0).getFitness());
        System.out.println("Conflicts: " + schedule.findConflicts());
 
        System.out.println();
        Class classes[] = schedule.getClasses();
        int idxClass = 1;
        for (Class bestClass : classes) {
            System.out.println("[Class " + idxClass + "]");
            System.out.println("Course: " + 
                    schedule.getCourse(bestClass.getCourseId()).getCourseName());
            System.out.println("Cohort: " + 
                    schedule.getCohort(bestClass.getCohortId()).getCohortId());
            System.out.println("Classroom: " + 
                    schedule.getClassroom(bestClass.getClassroomId()).getClassroomName());
            System.out.println("Instructor: " + 
                    schedule.getInstructor(bestClass.getInstructorId()).getInstructorName());
            System.out.println("Time: " + 
                    schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot());
            System.out.println();
            idxClass++;
        }
        solutions.storeSolution(schedule);
    }

    
	private static Schedule createSchedule() {
		
		Schedule schedule = new Schedule();

		schedule.addClassroom("B002", 35);
		schedule.addClassroom("B001",70);
		schedule.addClassroom("B200", 30);
		schedule.addClassroom("B210", 27);
		schedule.addClassroom("B211", 27);
		schedule.addClassroom("B307", 27);
		schedule.addClassroom("B311", 27);
		schedule.addClassroom("B310", 27);
		schedule.addClassroom("B100", 25);
		schedule.addClassroom("B101", 25);
		schedule.addClassroom("MB-06", 30);


		schedule.addScheduleslot("Mon 8:15 - 9:45");
		schedule.addScheduleslot("Mon 9:55 - 11:25");
		schedule.addScheduleslot("Mon 11:25 - 13:30");
		schedule.addScheduleslot("Mon 13:40 - 15:10");
		schedule.addScheduleslot("Mon 15:20 - 16:50");

		schedule.addScheduleslot("Tue 8:15 - 9:45");
		schedule.addScheduleslot("Tue 9:55 - 11:25");
		schedule.addScheduleslot("Tue 11:25 - 13:30");
		schedule.addScheduleslot("Tue 13:40 - 15:10");
		schedule.addScheduleslot("Tue 15:20 - 16:50");
		
		schedule.addScheduleslot("Wed 8:15 - 9:45");
		schedule.addScheduleslot("Wed 9:55 - 11:25");
		schedule.addScheduleslot("Wed 11:25 - 13:30");
		schedule.addScheduleslot("Wed 13:40 - 15:10");
		schedule.addScheduleslot("Wed 15:20 - 16:50");
		
		schedule.addScheduleslot("Thu 8:15 - 9:45");
		schedule.addScheduleslot("Thu 9:55 - 11:25");
		schedule.addScheduleslot("Thu 11:25 - 13:30");
		schedule.addScheduleslot("Thu 13:40 - 15:10");
		schedule.addScheduleslot("Thu 15:20 - 16:50");
		
		schedule.addScheduleslot("Fri 8:15 - 9:45");
		schedule.addScheduleslot("Fri 9:55 - 11:25");
		schedule.addScheduleslot("Fri 11:25 - 13:30");
		schedule.addScheduleslot("Fri 13:40 - 15:10");
		schedule.addScheduleslot("Fri 15:20 - 16:50");
		
		
		schedule.addInstructor("Lilia Sfaxi");
		schedule.addInstructor("Jihen Ben Naceur");
		schedule.addInstructor("Syrine Ben Meskina");
		schedule.addInstructor("Amine Ben Hassouna");
		schedule.addInstructor("Salma Hamza");
		schedule.addInstructor("Yassine Chaouch");
		schedule.addInstructor("Heger Arfaoui");
		schedule.addInstructor("Hatem Ayadi");
		schedule.addInstructor("Fadhel Jelassi");

		
		schedule.addCourse("CS303", "Operating Systems", new int[] {1, 2});
		schedule.addCourse("CS311", "Advanced Programming", new int[] {3, 4});
		schedule.addCourse("CS321", "Introduction to Engineering", new int[] {5, 6});
		schedule.addCourse("CS341", "Data Structures and Algorithms", new int[] {7, 8});
		schedule.addCourse("CS421", "Requirements and User Experience", new int[] {5});
		schedule.addCourse("MGMT101", "Introduction to Management", new int[] {9});
		
		
		schedule.addCohort(27, new int[] {1, 2, 3, 4, 5, 6});
		schedule.addCohort(24, new int[] {1, 2, 3, 4, 5, 6});
		schedule.addCohort(28, new int[] {1, 2, 3, 4, 5, 6});
		schedule.addCohort(21, new int[] {1, 2, 3, 4, 5, 6});
		schedule.addCohort(32, new int[] {1, 2, 3, 4, 5, 6});
		schedule.addCohort(20, new int[] {1, 2, 3, 4, 5, 6});
		schedule.addCohort(15, new int[] {1, 2, 3, 4, 5, 6});
		schedule.addCohort(33, new int[] {1, 2, 3, 4, 5, 6});
		schedule.addCohort(29, new int[] {1, 2, 3, 4, 5, 6});
		schedule.addCohort(25, new int[] {1, 2, 3, 4, 5, 6});

		return schedule;
	}
	
	 public static Object[][] getData(int n) {
	    Object[][]data = new Object[5][7];
	    data[0][0] ="    Monday";
	    data[1][0] ="     Tuesday";
	    data[2][0] ="     Wednesday";
	    data[3][0] ="     Thursday";
	    data[4][0] ="     Friday";
	    Class classes[] = schedule.getClasses();
	    int idxClass = 1;
	    for (Class bestClass : classes) {	        
	        if(schedule.getCohort(bestClass.getCohortId()).getCohortId() == n && (idxClass == (n-1)*6+1 || idxClass == (n-1)*6+2
	            || idxClass == (n-1)*6+3 || idxClass == (n-1)*6+4 || idxClass == (n-1)*6+5 || idxClass == (n-1)*6+6)) { 
	          if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 8:15 - 9:45")) {
	            data[0][1] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 9:55 - 11:25")) {
	            data[0][2] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 11:25 - 13:30")) {
	            data[0][3] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 13:40 - 15:10")) {
	            data[0][4] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 15:20 - 16:50")) {
	            data[0][5] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 8:15 - 9:45")) {
	            data[1][1] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 9:55 - 11:25")) {
	            data[1][2] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 11:25 - 13:30")) {
	            data[1][3] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 13:40 - 15:10")) {
	            data[1][4] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 15:20 - 16:50")) {
	            data[1][5] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 8:15 - 9:45")) {
	            data[2][1] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 9:55 - 11:25")) {
	            data[2][2] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 11:25 - 13:30")) {
	            data[2][3] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 13:40 - 15:10")) {
	            data[2][4] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 15:20 - 16:50")) {
	            data[2][5] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 8:15 - 9:45")) {
	            data[3][1] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 9:55 - 11:25")) {
	            data[3][2] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 11:25 - 13:30")) {
	            data[3][3] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 13:40 - 15:10")) {
	            data[3][4] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 15:20 - 16:50")) {
	            data[3][5] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 8:15 - 9:45")) {
	            data[4][1] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 9:55 - 11:25")) {
	            data[4][2] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 11:25 - 13:30")) {
	            data[4][3] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 13:40 - 15:10")) {
	            data[4][4] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 15:20 - 16:50")) {
	            data[4][5] = "<html><center>"+
	                 schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
	          + "<br>"
	          +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"</center></html>";
	          }
	          
	        }
	        idxClass++;
	    }
	    return data;
	  }
	 
	 public static Object[][] getDataForPDF(int n) {
     Object[][]data = new Object[5][7];
     data[0][0] ="Monday";
     data[1][0] ="Tuesday";
     data[2][0] ="Wednesday";
     data[3][0] ="Thursday";
     data[4][0] ="Friday";
     Class classes[] = schedule.getClasses();
     int idxClass = 1;
     for (Class bestClass : classes) {         
         if(schedule.getCohort(bestClass.getCohortId()).getCohortId() == n && (idxClass == (n-1)*6+1 || idxClass == (n-1)*6+2
             || idxClass == (n-1)*6+3 || idxClass == (n-1)*6+4 || idxClass == (n-1)*6+5 || idxClass == (n-1)*6+6)) { 
           if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 8:15 - 9:45")) {
             data[0][1] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 9:55 - 11:25")) {
             data[0][2] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 11:25 - 13:30")) {
             data[0][3] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 13:40 - 15:10")) {
             data[0][4] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Mon 15:20 - 16:50")) {
             data[0][5] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 8:15 - 9:45")) {
             data[1][1] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 9:55 - 11:25")) {
             data[1][2] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 11:25 - 13:30")) {
             data[1][3] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 13:40 - 15:10")) {
             data[1][4] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Tue 15:20 - 16:50")) {
             data[1][5] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 8:15 - 9:45")) {
             data[2][1] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 9:55 - 11:25")) {
             data[2][2] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 11:25 - 13:30")) {
             data[2][3] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 13:40 - 15:10")) {
             data[2][4] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Wed 15:20 - 16:50")) {
             data[2][5] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 8:15 - 9:45")) {
             data[3][1] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 9:55 - 11:25")) {
             data[3][2] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 11:25 - 13:30")) {
             data[3][3] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 13:40 - 15:10")) {
             data[3][4] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Thu 15:20 - 16:50")) {
             data[3][5] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 8:15 - 9:45")) {
             data[4][1] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 9:55 - 11:25")) {
             data[4][2] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 11:25 - 13:30")) {
             data[4][3] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 13:40 - 15:10")) {
             data[4][4] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           else if(schedule.getScheduleslot(bestClass.getScheduleslotId()).getScheduleslot().equals("Fri 15:20 - 16:50")) {
             data[4][5] = ""+
                  schedule.getInstructor(bestClass.getInstructorId()).getInstructorName()
           + "\n"
           +schedule.getClassroom(bestClass.getClassroomId()).getClassroomName()+"";
           }
           
         }
         idxClass++;
     }
     return data;
   }
	
}
