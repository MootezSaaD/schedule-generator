package io.algorithm;

public class GA {

	private int popSize;
	private double mutationPerc;
	private double crossoverPerc;
	private int elitesNumber;
	protected int trnmtCapacity;

	public GA(int popSize, double mutationPerc, double crossoverPerc, int elitesNumber,
			int trnmtCapacity) {
		this.mutationPerc = mutationPerc;
		this.popSize = popSize;
		this.elitesNumber = elitesNumber;
		this.trnmtCapacity = trnmtCapacity;
		this.crossoverPerc = crossoverPerc;
	}

	public Population firstPopulation(Schedule schedule) {
		Population pop = new Population(this.popSize, schedule);
		return pop;
	}

	public boolean hasReachedMaxGen(int genCount, int maxGen) {
		return (genCount > maxGen);
	}

	public boolean hasReachedMaxFitness(Population pop) {
		return pop.getMostFit(0).getFitness() == 1.0;
	}

	public double computeFitness(Individual indv, Schedule schedule) {		
		schedule.generateClasses(indv);	
		int penalties = schedule.findConflicts();
		double fitness = 1 / (double) (penalties + 1);
		indv.setFitness(fitness);
		return fitness;
	}

	public void assessPop(Population population, Schedule schedule) {
		double popFit = 0;
		for (Individual individual : population.getIndivs()) {
			popFit += this.computeFitness(individual, schedule);
		}
		population.setPopFitness(popFit);
	}

	public Individual getTournamentFittest(Population pop) {
		Population trnmt = new Population(this.trnmtCapacity);

		pop.shuffle();
		for (int i = 0; i < this.trnmtCapacity; i++) {
			Individual trnmtIndv = pop.getIdv(i);
			trnmt.setIdv(i, trnmtIndv);
		}
		return trnmt.getMostFit(0);
	}


	public Population mutatePop(Population pop, Schedule schedule) {
		Population newPop = new Population(this.popSize);
		for (int idx = 0; idx < pop.size(); idx++) {
			Individual indv = pop.getMostFit(idx);
			Individual rndmIndv = new Individual(schedule);
			for (int idxGene = 0; idxGene < indv.getLenChromosome(); idxGene++) {
				if (idx > this.elitesNumber) {
					if (this.mutationPerc > Math.random()) {	
						indv.setGene(idxGene, rndmIndv.getGene(idxGene));
					}
				}
			}	
			newPop.setIdv(idx, indv);
		}
		return newPop;
	}

	public Population crossover(Population pop) {
		Population newPop = new Population(pop.size());
		for (int idx = 0; idx < pop.size(); idx++) {
			Individual fittestParent = pop.getMostFit(idx);
			if (this.crossoverPerc > Math.random() && idx >= this.elitesNumber) {
				Individual child = new Individual(fittestParent.getLenChromosome());
				Individual fittestParentTrnmt = getTournamentFittest(pop);

				for (int idxGene = 0; idxGene < fittestParent.getLenChromosome(); idxGene++) {
					if (0.5 > Math.random()) {
						child.setGene(idxGene, fittestParent.getGene(idxGene));
					} else {
						child.setGene(idxGene, fittestParentTrnmt.getGene(idxGene));
					}
				}
				newPop.setIdv(idx, child);
			} else {

				newPop.setIdv(idx, fittestParent);
			}
		}

		return newPop;
	}
}
