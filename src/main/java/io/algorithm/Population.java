package io.algorithm;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Population {
	
	private double currentPopFitness = -1;
	private Individual populationArr[];
	
	public Population(int pSize) {
		this.populationArr = new Individual[pSize];
	}
	
	public Population(int pSize, Schedule schedule) {
		this.populationArr = new Individual[pSize];
		for (int idxIdv = 0; idxIdv < pSize; idxIdv++) {
			Individual idv = new Individual(schedule);
			this.populationArr[idxIdv] = idv;
		}
	}

	public Population(int pSize, int lenChromosome) {
		this.populationArr = new Individual[pSize];
		for (int individualCount = 0; individualCount < pSize; individualCount++) {
			Individual individual = new Individual(lenChromosome);
			this.populationArr[individualCount] = individual;
		}
	}

	public Individual[] getIndivs() {
		return this.populationArr;
	}

	public Individual getMostFit(int idx) {
		Arrays.sort(this.populationArr, new Comparator<Individual>() {
			public int compare(Individual idv1, Individual idv2) {
				if (idv1.getFitness() > idv2.getFitness()) {
					return -1;
				} else if (idv1.getFitness() < idv2.getFitness()) {
					return 1;
				}
				return 0;
			}
		});
		return this.populationArr[idx];
	}

	
	public void setPopFitness(double newFitness) {
		this.currentPopFitness = newFitness;
	}

	
	public double getPopFitness() {
		return this.currentPopFitness;
	}

	
	public int size() {
		return this.populationArr.length;
	}

	
	public Individual setIdv(int idx, Individual idv) {
		return populationArr[idx] = idv;
	}

	
	public Individual getIdv(int idx) {
		return populationArr[idx];
	}

	
	public void shuffle() {
		Random rnd = new Random();
		for (int i = populationArr.length - 1; i > 0; i--) {
			int idx = rnd.nextInt(i + 1);
			Individual a = populationArr[idx];
			populationArr[idx] = populationArr[i];
			populationArr[i] = a;
		}
	}

}