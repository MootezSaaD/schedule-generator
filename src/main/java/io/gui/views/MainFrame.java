package io.gui.views;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import io.algorithm.RunConsole;
import io.repository.ScheduleRepository;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
  private JPanel contentPane;
  private JComboBox<Integer> grpComboList;
  private static JTable tableOfProfessors;
  private String[] columnNamesProfessors = { "Name" };
  private String[] columnNamesModules = { "Code", "Name", "Professors IDs" };
  private String[] columnNamesRooms = { "Name", "Capacity" };
  private DefaultTableModel prfTableModel;
  private DefaultTableModel prfTableModel2;
  private DefaultTableModel crsTableModel;
  private DefaultTableModel crsTableModel2;
  private DefaultTableModel clsrmsTableModel;
  private DefaultTableModel clsrmsTableModel2;
  public static DefaultTableModel scheduleModel = new DefaultTableModel();
  public static DefaultTableModel scheduleModel2;
  private Object[][] prfData;
  private Object[][] modulesData;
  private Object[][] roomsData;
  private ScheduleRepository timetableRepository;
  private JTable tablfOfCourses;
  private JTable tableOfClassrooms;
  public static JTable table = new JTable();
  public static String[] scheduleHours = { " ", "8.15-9.45", "9.55-11.25",
      "12-1.30", "1.40-3.10", "3.20-4.50", "5-6.30" };
  public static Object[][] scheduleData = new Object[5][7];
  public Document document;
  public PdfWriter writer;

  /**
   * Create the frame.
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public MainFrame() {
    setTitle("Schedule Generator");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 100, 700);
    setSize(600, 700);
    setMaximumSize(new Dimension(600, 700));

    JMenuBar menuBar = new JMenuBar();
    setJMenuBar(menuBar);

    JMenu mnFile = new JMenu("File");
    menuBar.add(mnFile);

    JMenuItem mntmNewSession = new JMenuItem("New Session");
    mnFile.add(mntmNewSession);

    JMenuItem mntmOpenSession = new JMenuItem("Open Session");
    mnFile.add(mntmOpenSession);

    JMenuItem mntmSaveSession = new JMenuItem("Save Session");
    mnFile.add(mntmSaveSession);

    JMenuItem mntmExport = new JMenuItem("Export");
    mnFile.add(mntmExport);

    JMenuItem mntmExit = new JMenuItem("Exit");
    mnFile.add(mntmExit);

    JMenu mnEdit = new JMenu("Edit");
    menuBar.add(mnEdit);

    JMenuItem mntmAddProfessor = new JMenuItem("Add Professor");
    mntmAddProfessor.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        AddProfessorFrame tf = new AddProfessorFrame();
        tf.setVisible(true);
      }
    });

    mnEdit.add(mntmAddProfessor);

    JMenuItem mntmAddProfessorContraints = new JMenuItem(
        "Add Professor Contraints");
    mnEdit.add(mntmAddProfessorContraints);

    JMenuItem mntmAddCourse = new JMenuItem("Add Course");
    mnEdit.add(mntmAddCourse);

    JMenuItem mntmAddCourseConstraints = new JMenuItem(
        "Add Course Constraints");
    mnEdit.add(mntmAddCourseConstraints);

    JMenuItem mntmAddClassroom = new JMenuItem("Add Classroom");
    mnEdit.add(mntmAddClassroom);

    JMenuItem mntmAddClassroomConstraint = new JMenuItem(
        "Add Classroom Constraint");
    mnEdit.add(mntmAddClassroomConstraint);

    JMenu mnHelp = new JMenu("Help");
    menuBar.add(mnHelp);

    JMenuItem mntmAbout = new JMenuItem("About");
    mntmAbout.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        String message = "<html>"
            + "<p>Schedule Generator is part of the CS321 cours @ MedTech.</p>"
            + "<center>" + "<p>Made By:</p>" + "<p>Marwen Dallel.</p>"
            + "<p>Mootez Saad.</p>" + "<p>Mahdi Turki.</p>"
            + "<p>Zeineb Moalla.</p>" + "</center>" + "</html>";

        JOptionPane.showMessageDialog(null, message, "About", 1);
      }
    });
    mnHelp.add(mntmAbout);
    contentPane = new JPanel();
    contentPane.setSize(this.getWidth(), this.getHeight());
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    JPanel schedPanel = new JPanel();
    scheduleData[0][0] = "    Monday";
    scheduleData[1][0] = "     Tuesday";
    scheduleData[2][0] = "     Wednesday";
    scheduleData[3][0] = "     Thursday";
    scheduleData[4][0] = "     Friday";

    scheduleModel = new DefaultTableModel(scheduleData, scheduleHours);
    table = new JTable(scheduleModel);

    table.setRowHeight(70);
    table.setRowSelectionAllowed(false);
    schedPanel.add(table);
    JScrollPane schedulesc = new JScrollPane(table);
    schedPanel.add(schedulesc);

    GridBagLayout gbl_contentPane = new GridBagLayout();
    gbl_contentPane.columnWidths = new int[] { 494, 494, 0 };
    gbl_contentPane.rowHeights = new int[] { 120, 0 };
    gbl_contentPane.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
    gbl_contentPane.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
    contentPane.setLayout(gbl_contentPane);

    prfTableModel = new DefaultTableModel(prfData, columnNamesProfessors);

    crsTableModel = new DefaultTableModel(prfData, columnNamesModules);

    clsrmsTableModel = new DefaultTableModel(roomsData, columnNamesRooms);

    JPanel resPanel = new JPanel();
    // resPanel.setMaximumSize(new
    // Dimension(this.getWidth()/2,this.getHeight()));
    GridBagConstraints gbc_resPanel = new GridBagConstraints();
    gbc_resPanel.anchor = GridBagConstraints.NORTH;
    gbc_resPanel.fill = GridBagConstraints.HORIZONTAL;
    gbc_resPanel.insets = new Insets(0, 0, 0, 5);
    gbc_resPanel.gridx = 0;
    gbc_resPanel.gridy = 0;
    contentPane.add(resPanel, gbc_resPanel);
    resPanel.setLayout(new FormLayout(
        new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC,
            ColumnSpec.decode("default:grow"), },
        new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("default:grow"), FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("max(100dlu;min)"), FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("max(100dlu;min)"), FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("max(100dlu;min)"), FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("max(14dlu;default)"), FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("max(21dlu;default)"), FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("default:grow"), }));

    JPanel groupsPanel = new JPanel();
    groupsPanel.setBorder(BorderFactory
        .createTitledBorder(BorderFactory.createTitledBorder("Groups")));
    resPanel.add(groupsPanel, "2, 2, fill, fill");
    groupsPanel.setLayout(new FormLayout(
        new ColumnSpec[] { ColumnSpec.decode("223px"),
            ColumnSpec.decode("37px"), FormSpecs.RELATED_GAP_COLSPEC,
            FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
            FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
            FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
            ColumnSpec.decode("max(64dlu;default)"),
            FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
            FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, },
        new RowSpec[] { FormSpecs.LINE_GAP_ROWSPEC, RowSpec.decode("20px"), }));

    grpComboList = new JComboBox();
    groupsPanel.add(grpComboList, "12, 2, left, top");
    grpComboList.setModel(new DefaultComboBoxModel(
        new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
    grpComboList.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        Object[][] data = RunConsole
            .getData(Integer.parseInt((String) grpComboList.getSelectedItem()));
        scheduleModel.setDataVector(data, scheduleHours);
        table.updateUI();
      }
    });

    JPanel professorsPanel = new JPanel();
    resPanel.add(professorsPanel, "2, 4, fill, fill");
    professorsPanel.setBorder(BorderFactory
        .createTitledBorder(BorderFactory.createTitledBorder("Professors")));
    professorsPanel.setLayout(new FormLayout(
        new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC,
            ColumnSpec.decode("default:grow"), },
        new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC,
            FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("max(79dlu;default)"), FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("default:grow"), }));

    JPanel panelTableOfProfs = new JPanel();
    professorsPanel.add(panelTableOfProfs, "2, 4, fill, fill");
    panelTableOfProfs.setLayout(new GridLayout(1, 0, 0, 0));
    tableOfProfessors = new JTable(prfTableModel);
    tableOfProfessors.setAutoCreateRowSorter(true);
    JScrollPane scrollPaneProfessors = new JScrollPane(tableOfProfessors);
    panelTableOfProfs.add(scrollPaneProfessors);

    JPanel btnPane = new JPanel();
    professorsPanel.add(btnPane, "2, 6, fill, fill");
    btnPane.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));

    JButton imprtProfessorsBtn = new JButton("Import");
    imprtProfessorsBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        timetableRepository = ScheduleRepository.getInstance();
        timetableRepository.readFromCSVProf();
        prfData = timetableRepository.prof2Table();
        prfTableModel2 = new DefaultTableModel(prfData, columnNamesProfessors);
        tableOfProfessors.setModel(prfTableModel2);
      }
    });

    btnPane.add(imprtProfessorsBtn);

    JButton editProfessorBtn = new JButton("Edit");
    btnPane.add(editProfessorBtn);

    JPanel coursesPanel = new JPanel();
    resPanel.add(coursesPanel, "2, 6, fill, fill");
    coursesPanel.setBorder(BorderFactory
        .createTitledBorder(BorderFactory.createTitledBorder("Courses")));
    coursesPanel.setLayout(new FormLayout(
        new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC,
            ColumnSpec.decode("default:grow"), },
        new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("max(73dlu;default)"), FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("default:grow"), }));

    JPanel panelTableOfCourses = new JPanel();
    coursesPanel.add(panelTableOfCourses, "2, 2, fill, fill");
    panelTableOfCourses.setLayout(new GridLayout(1, 0, 0, 0));
    tablfOfCourses = new JTable(crsTableModel);
    tablfOfCourses.setAutoCreateRowSorter(true);
    JScrollPane scrollPaneCourses = new JScrollPane(tablfOfCourses);
    panelTableOfCourses.add(scrollPaneCourses);

    JPanel btnPanelCourses = new JPanel();
    btnPanelCourses.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
    coursesPanel.add(btnPanelCourses, "2, 4, fill, fill");

    JButton imprtCoursesBtn = new JButton("Import");
    imprtCoursesBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        timetableRepository = (ScheduleRepository) ScheduleRepository
            .getInstance();
        timetableRepository.readFromCSVModule();
        modulesData = timetableRepository.module2Table();
        crsTableModel2 = new DefaultTableModel(modulesData, columnNamesModules);
        tablfOfCourses.setModel(crsTableModel2);
      }
    });
    btnPanelCourses.add(imprtCoursesBtn);

    JButton editCourseBtn = new JButton("Edit");
    btnPanelCourses.add(editCourseBtn);

    JPanel classroomsPanel = new JPanel();
    resPanel.add(classroomsPanel, "2, 8, fill, fill");
    classroomsPanel.setBorder(BorderFactory
        .createTitledBorder(BorderFactory.createTitledBorder("Classrooms")));
    classroomsPanel.setLayout(new FormLayout(
        new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC,
            ColumnSpec.decode("default:grow"), },
        new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("max(79dlu;default)"), FormSpecs.RELATED_GAP_ROWSPEC,
            RowSpec.decode("default:grow"), }));

    JPanel panelClassRooms = new JPanel();
    classroomsPanel.add(panelClassRooms, "2, 2, fill, fill");
    panelClassRooms.setLayout(new GridLayout(1, 0, 0, 0));
    tableOfClassrooms = new JTable(clsrmsTableModel);
    tableOfClassrooms.setAutoCreateRowSorter(true);
    JScrollPane scrollPaneClsrooms = new JScrollPane(tableOfClassrooms);
    panelClassRooms.add(scrollPaneClsrooms);

    JPanel btnPanelClassrooms = new JPanel();
    btnPanelClassrooms.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
    classroomsPanel.add(btnPanelClassrooms, "2, 4, fill, fill");

    JButton imprtClassroomBtn = new JButton("Import");
    imprtClassroomBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        timetableRepository = (ScheduleRepository) ScheduleRepository
            .getInstance();
        timetableRepository.readFromCSVRoom();
        roomsData = timetableRepository.room2Table();
        clsrmsTableModel2 = new DefaultTableModel(roomsData, columnNamesRooms);
        tableOfClassrooms.setModel(clsrmsTableModel2);
      }
    });
    btnPanelClassrooms.add(imprtClassroomBtn);

    JButton btnClassroomBtn = new JButton("Edit");
    btnPanelClassrooms.add(btnClassroomBtn);

    JPanel btnMainPanel = new JPanel();
    resPanel.add(btnMainPanel, "2, 10, fill, fill");

    JButton btnGenerate = new JButton("Generate");
    btnGenerate.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        // JOptionPane.showMessageDialog(null, "LOL", "LOL", 1);
        RunConsole.main();
      }
    });
    btnMainPanel.add(btnGenerate);
    btnMainPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));

    /*
     * schedPanel.setLayout(new GridLayout(6,7)); JLabel[][] schedule = new
     * JLabel[6][7];
     * 
     * schedule[0][0] = new JLabel("0"); schedPanel.add(schedule[0][0]);
     * 
     * schedule[0][1] = new JLabel("1"); schedPanel.add(schedule[0][1]);
     * 
     * schedule[0][2] = new JLabel("2"); schedPanel.add(schedule[0][2]); /*
     * for(int i=0; i<7; i++) { schedule[0][i] = new JLabel();
     * schedule[0][i].setText(""+i); schedPanel.add(schedule[0][i]); }
     */
    schedPanel.setBorder(BorderFactory
        .createTitledBorder(BorderFactory.createTitledBorder("Schedule")));
    GridBagConstraints gbc_schedPanel = new GridBagConstraints();
    gbc_schedPanel.anchor = GridBagConstraints.NORTH;
    gbc_schedPanel.fill = GridBagConstraints.HORIZONTAL;
    gbc_schedPanel.gridx = 1;
    gbc_schedPanel.gridy = 0;
    // schedPanel.setMaximumSize(new Dimension(this.getWidth()/2,
    // this.getHeight()));
    contentPane.add(schedPanel, gbc_schedPanel);
    schedPanel.setLayout(new GridLayout(1, 0, 0, 0));
    JButton exportJPGBtn = new JButton("Export As JPG");
    exportJPGBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        saveToImage(table, table.getTableHeader(),
            grpComboList.getSelectedIndex() + 1);
      }
    });
    groupsPanel.add(exportJPGBtn, "10, 2");

    JButton exportPDFBtn = new JButton("Export As PDF");
    exportPDFBtn.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        saveToPDF(table, grpComboList.getSelectedIndex()+1);
      }
    });
    groupsPanel.add(exportPDFBtn, "8, 2");

    JScrollPane jsp = new JScrollPane(contentPane);
    setContentPane(jsp);

    pack();
  }

  // Updating Profs Table
  public static void addRowToTableProfs(Object[] data) {
    DefaultTableModel newmdl = (DefaultTableModel) tableOfProfessors.getModel();
    newmdl.addRow(data);
  }

  /*
   * Exporting to JPG
   */
  private static void saveToImage(JTable table, JTableHeader header, int n) {
    int w = Math.max(table.getWidth(), header.getWidth());
    int h = table.getHeight() + header.getHeight();
    BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = bi.createGraphics();
    header.paint(g2);
    g2.translate(0, header.getHeight());
    table.paint(g2);
    g2.dispose();
    try {
      ImageIO.write(bi, "png", new File("tableImage" + n + ".png"));
    } catch (IOException ioe) {
      System.out.println("write: " + ioe.getMessage());
    }
  }

  private static void saveToPDF(JTable table, int n) {
    try {
      Document doc = new Document();
      Paragraph grpNbr = new Paragraph();
      Object[][] cleanData = new Object[table.getRowCount()][table.getColumnCount()];
      cleanData = RunConsole.getDataForPDF(n);
      PdfWriter.getInstance(doc, new FileOutputStream("Group N " + n + ".pdf"));
      doc.open();
      PdfPTable pdfTable = new PdfPTable(table.getColumnCount());
      pdfTable.setWidthPercentage(100);
      // adding table headers
      for (int i = 0; i < table.getColumnCount(); i++) {
        pdfTable.addCell(table.getColumnName(i));
      }

      // extracting data from the JTable and inserting it to PdfPTable
      for (int rows = 0; rows < table.getRowCount(); rows++) {
        for (int cols = 0; cols < table.getColumnCount(); cols++) {
          if (cleanData[rows][cols] != null) {
            pdfTable.addCell(cleanData[rows][cols].toString());
          } else {
            pdfTable.addCell(" ");
          }

        }
      }
      Paragraph paragraph = new Paragraph("Group number "+n);
      Paragraph paragraph2 = new Paragraph("MedTech-SMU 2019/2020");
      doc.add(paragraph);
      doc.add(Chunk.NEWLINE);
      doc.add(paragraph2);
      doc.add(Chunk.NEWLINE);
      doc.add(pdfTable);
      doc.close();
      System.out.println("Group number "+n+" 's schedule has been successfuly generated");
    } catch (DocumentException ex) {

    } catch (FileNotFoundException ex) {

    }
  }
}
