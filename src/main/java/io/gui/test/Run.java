package io.gui.test;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import io.gui.views.MainFrame;

public class Run {

	public static void main(String[] args) {

		try {
			// Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			// handle exception
		} catch (ClassNotFoundException e1) {
			// handle exception
		} catch (InstantiationException e2) {
			// handle exception
		} catch (IllegalAccessException e3) {
			// handle exception
		}

		MainFrame mf = new MainFrame();
		mf.setVisible(true);
	}

}
