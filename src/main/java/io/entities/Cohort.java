package io.entities;


public class Cohort {
    private int cohortId;
    private static int increment = 1;
    private final int cohortSize;
    private final int courseIds[];

    
    public Cohort(int cohortSize, int courseIds[]){
        this.cohortId = increment++;
        this.cohortSize = cohortSize;
        this.courseIds = courseIds;
    }
    
    
    public int getCohortId(){
        return this.cohortId;
    }
    
    
    public int getCohortSize(){
        return this.cohortSize;
    }
        
    
    public int[] getCourseIds(){
        return this.courseIds;
    }
}
