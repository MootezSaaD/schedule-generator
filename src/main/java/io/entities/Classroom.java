package io.entities;

public class Classroom {
	private int classroomId;
    private static int increment = 1;
	private final String classroomName;
	private final int capacity;

	public Classroom(String classroomName, int capacity) {
		this.classroomId = increment++;
		this.classroomName = classroomName;
		this.capacity = capacity;
	}

	public int getClassroomId() {
		return this.classroomId;
	}

	public String getClassroomName() {
		return this.classroomName;
	}

	public int getClassroomCapacity() {
		return this.capacity;
	}
}