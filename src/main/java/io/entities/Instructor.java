package io.entities;


public class Instructor {
    private int instructorId;
    private static int increment = 1;
    private final String instructorName;
  
    public Instructor(String name) {
        this.instructorId = increment++;
        this.instructorName = name;
    }
    
    public int getInstructorId(){
        return this.instructorId;
    }
    
    public String getInstructorName(){
        return this.instructorName;
    }
	
}
