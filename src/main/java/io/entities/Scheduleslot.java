package io.entities;


public class Scheduleslot {
    private int scheduleslotId;
    private static int increment = 1;
    private final String scheduleslot;

    public Scheduleslot(String scheduleslot){
        this.scheduleslotId = increment++;
        this.scheduleslot = scheduleslot;
    }
    
    public int getScheduleslotId(){
        return this.scheduleslotId;
    }
    
    public String getScheduleslot(){
        return this.scheduleslot;
    }
}
