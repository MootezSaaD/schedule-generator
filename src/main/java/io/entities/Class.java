package io.entities;


public class Class {
	private final int classId;
	private final int cohortId;
	private final int courseId;
	private int instructorId;
	private int scheduleslotId;
	private int classroomId;


	public Class(int classId, int cohortId, int courseId){
		this.classId = classId;
		this.courseId = courseId;
		this.cohortId = cohortId;
	}


	public void addInstructor(int instructorId) {
		this.instructorId = instructorId;
	}


	public void addScheduleslot(int scheduleslotId){
		this.scheduleslotId = scheduleslotId;
	}    


	public void setClassroomId(int classroomId){
		this.classroomId = classroomId;
	}


	public int getClassId(){
		return this.classId;
	}


	public int getCohortId(){
		return this.cohortId;
	}


	public int getCourseId(){
		return this.courseId;
	}


	public int getInstructorId(){
		return this.instructorId;
	}


	public int getScheduleslotId(){
		return this.scheduleslotId;
	}


	public int getClassroomId(){
		return this.classroomId;
	}
}

