package io.entities;

public class Course {
    private int courseId;
    private static int increment = 1;
    private final String courseCode;
    private final String course;
    private final int instructorIds[];
    
    public Course(String courseCode, String course, int instructorIds[]){
        this.courseId = increment++;
        this.course = course;
        this.instructorIds = instructorIds;
        this.courseCode = courseCode;
    }
    
    public int getCourseId(){
        return this.courseId;
    }
    
    public String getCourseCode(){
        return this.courseCode;
    }
    
    public String getCourseName(){
        return this.course;
    }
    
    public int fetchRandomInstructorId(){
        int instructorId = instructorIds[(int) (instructorIds.length * Math.random())];
        return instructorId;
    }
}
